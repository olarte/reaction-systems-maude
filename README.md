# ccReact: An Interacting Language for Reaction Systems

Reaction Systems (RSs) are a successful computational framework inspired by
biological systems, where entities are produced by reactions and can be used to
enable or inhibit other reactions. In RSs interaction with the environment is
modeled by a linear sequence of sets of entities called context. 

_ccReact_ is an interaction language for RSs that extends the classic RS
computational model by allowing the specification of possibly recursive,
nondeterministic conditional context sequences, enhancing the interactive
capabilities of the models. This repository contains the specification of
_ccReact_ in the rewriting system [Maude](http://maude.cs.illinois.edu/). Our
implementation allows to:

* Simulate the RS by rewriting;
* Check reachability properties using the Maude's `search` command; and 
* Verify CTL and LTL properties using [umaudemc](https://github.com/fadoss/umaudemc).
 
Details about _ccReact_ can be found in this [CMSB paper](./CMSB.pdf) [1].


## Getting started

The project was tested in [Maude 3.2.1](http://maude.cs.illinois.edu/) and with
[umaudemc](https://github.com/fadoss/umaudemc) for model checking LTL and CTL
temporal formulas. 

## Files

- `syntax`: Basic sorts and operators to define reactions, contexts and
  _ccReact_ processes. 
- `semantics`: Rules defining the evaluation of contexts and how the system
  evolve.

The directly `case-studies` contains some simple examples to illustrate the
definition of RSs and the specification of properties. This directory also
contains different analyses concerning the protein signaling networks in the
HER2-positive subtype breast cancer. 

Additional information about the case studies and the Maude specifications can be 
found in this [paper](./CMSB.pdf) and the header of the files.

## References

[1] Demis Ballis, Linda Brodo, Moreno Falaschi and  Carlos Olarte: _Process
calculi and rewriting techniques for analyzing reaction systems_. In:
Computational Methods in Systems Biology (CMSB’24), Springer Nature
Switzerland. Pisa, 2024. 
