# Case Studies

To following files present very simple examples to understand the syntax of
_ccReact_ and the Maude's commands that can be used to verify temporal properties
of the model:

- `example1.maude` (Example 1 in the [CMSB paper](../CMSB.pdf))
- `simple-example2.maude`
- `simple-example.maude`

The case studies considering the three different kinds of breast cancer,
identified as BT474, HCC1954, and SKBR3, can be found in the files with the
same name for the sort-term experiments. For the long-term experiments, the
suffix `-ext` is added to the name of the file. In the end of each of the
following Maude's file, different queries using the command `search` and
`umaudemc` for LTL/CTL model checking can be found. Some of these queries are
explained in the [paper](../CMSB.pdf):

- `BT474-ext.maude`
- `BT474.maude`
- `HCC1954-ext.maude`
- `HCC1954.maude`
- `SKBR3-ext.maude`
- `SKBR3.maude`


The file `ifn-case.maude` contains the system explained in [1]. Furthermore,
the file `example-CTL.maude` shows how the CTL queries in the case study
considered in [2] can be reproduced within our framework. 

## References


- [1] Linda Brodo, Roberto Bruni, Moreno Falaschi, Roberta Gori, Francesca
  Levi, Paolo Milazzo: Quantitative extensions of reaction systems based on SOS
  semantics. Neural Comput. Appl. 35(9): 6335-6359 (2023)

- [2] Artur Meski, Wojciech Penczek, Grzegorz Rozenberg: Model checking
  temporal properties of reaction systems. Inf. Sci. 313: 22-42 (2015)

